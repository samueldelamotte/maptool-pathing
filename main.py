import os
import functools
import sys
import utils.highways as hw
import json
import configparser
import shutil
from pathlib import Path
from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets, QtWebChannel

class OriginDestinationWindow(QtWidgets.QWidget):
    def __init__(self, config, graph, bbox):
        super(OriginDestinationWindow, self).__init__()
        self.config = config
        self.graph = graph
        self.bbox = bbox
        self.traffic_pop_percent = 100.0
        self.populationStorage = 0.0
        self.textStorage = ""
        self.configPath = os.path.join('utils', 'pathGen', 'Graph', 'config.ini')
        self.setupUi()

    def append(self, text):
        cursor = self.textOut.textCursor()
        cursor.movePosition(cursor.End)
        cursor.insertText(text)
        self.textOut.ensureCursorVisible()

    def stdoutReady(self):
        text = str(self.process.readAllStandardOutput(), encoding='utf-8')
        print(text.strip())
        self.append(text)

    def stderrReady(self):
        text = str(self.process.readAllStandardError(), encoding='utf-8')
        print(text.strip())
        self.append(text)

    def setupUi(self):
        self.setWindowTitle("Map Generation Tool - [Select OD Pairs]")
        self.setMinimumSize(1200, 900)
        layout = QtWidgets.QGridLayout()
        layout.setSpacing(10)
        self.setLayout(layout)

        # Remaining traffic population label
        trafficPopRemaining = self.trafficPopRemaining = QtWidgets.QLabel()
        trafficPopRemaining.setText("REMAINING TRAFFIC POPULATION % = 100.0")
        layout.addWidget(trafficPopRemaining, 0, 3, 1, 3)

        # Sets up the OSM html / javascript map as a widget
        view = self.view = QtWebEngineWidgets.QWebEngineView()
        channel = self.channel = QtWebChannel.QWebChannel()
        channel.registerObject("OriginDestinationWindow", self)
        view.page().setWebChannel(channel)
        file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "src/main/assets/map2.html",
        )
        self.view.setUrl(QtCore.QUrl.fromLocalFile(file))
        self.view.load(QtCore.QUrl.fromLocalFile(file))
        layout.addWidget(view, 1, 3, 30, 30)

        # Config label
        mapNameLabel = self.mapNameLabel = QtWidgets.QLabel()
        mapNameLabel.setText("CONFIG FILE OUTPUT: ")
        layout.addWidget(mapNameLabel, 0, 0, 1, 3)

        # Config textbox
        configTextBox = self.configTextBox = QtWidgets.QTextEdit()
        configTextBox.setFixedWidth(350)
        configTextBox.setReadOnly(True)
        layout.addWidget(configTextBox, 1, 0, layout.rowCount()-2, 3)

        removeOdPairBtn = self.removeOdPairBtn = QtWidgets.QPushButton("Remove Last OD Pair!")
        removeLastPair = functools.partial(self.removeLastPair)
        removeOdPairBtn.clicked.connect(removeLastPair)
        layout.addWidget(removeOdPairBtn, layout.rowCount()-1, 0, 1, 2)
        removeOdPairBtn.setEnabled(False)

        calcPaths = self.calcPaths = QtWidgets.QPushButton("Finish!")
        generateAiPathing = functools.partial(self.generateAiPathing)
        calcPaths.clicked.connect(generateAiPathing)
        layout.addWidget(calcPaths, layout.rowCount()-1, 2, 1, 1)

        # Console textbox
        textOut = self.textOut = QtWidgets.QTextEdit()
        textOut.setFixedHeight(100)
        textOut.setReadOnly(True)
        textOut.ensureCursorVisible()
        layout.addWidget(textOut, layout.rowCount(), 0, 1, layout.columnCount())

    def removeLastPair(self):
        page = self.view.page()
        page.runJavaScript("map.removeLayer(originMarker);")
        page.runJavaScript("map.removeLayer(destinationMarker);")
        page.runJavaScript("map.removeLayer(spawnCircle);")
        page.runJavaScript("map.removeLayer(joinLine);")
        self.configTextBox.setPlainText(self.textStorage)
        if (self.traffic_pop_percent != 100.0 and self.populationStorage != 0.0):
            page.runJavaScript(r"if (pairNum != 1) {pairNum -= 1;}")
            self.traffic_pop_percent += self.populationStorage
            self.trafficPopRemaining.setText("REMAINING TRAFFIC POPULATION % = {0}".format(str(self.traffic_pop_percent)))
            page.runJavaScript("trafficPopPercentRemaining += {0}".format(self.populationStorage))
            self.populationStorage = 0.0
            self.config.remove_section(self.config.sections()[-1])
            with open(self.configPath, 'w') as configFile:
                self.config.write(configFile)
    
    def generateAiPathing(self):
        self.process = QtCore.QProcess(self)
        self.process.readyReadStandardOutput.connect(self.stdoutReady)
        self.process.readyReadStandardError.connect(self.stderrReady)
        self.process.started.connect(lambda: self.disableUi())
        self.process.finished.connect(lambda: self.enableUi())
        pathGenPath = os.path.join('utils', 'pathGen', 'pathGen.py')
        if sys.platform.startswith('win32'):
            self.process.start(os.path.join("windows-env", "python.exe"), [pathGenPath])
        elif sys.platform.startswith('darwin'):
            self.process.start('mac-env/bin/python', [pathGenPath])
        elif sys.platform.startswith('linux'):
            self.process.start('linux-env/bin/python', [pathGenPath])

    def disableUi(self):
        self.calcPaths.setEnabled(False)
        self.removeOdPairBtn.setEnabled(False)
        self.view.setEnabled(False)
        page = self.view.page()
        page.runJavaScript(r"swal({text: 'Calculating paths... Progress will be displayed in the console below.', showConfirmButton: false});")


    def enableUi(self):
        Path(os.path.join("Resources", "Maps")).mkdir(parents=True, exist_ok=True)
        shutil.move(os.path.join(self.config["MAP_ATTRIBUTES"]["name"]), os.path.join("Resources", "Maps"))
        self.calcPaths.setEnabled(True)
        self.removeOdPairBtn.setEnabled(True)
        self.view.setEnabled(True)
        page = self.view.page()
        page.runJavaScript(r"swal({text: 'Calculating paths... Done!'});")

    @QtCore.pyqtSlot()
    def enableRemoveODPairBtn(self):
        self.removeOdPairBtn.setEnabled(True)
        page = self.view.page()
        page.runJavaScript(r"swal({text: 'Please specify the OD pairs for the game traffic.'});")

    @QtCore.pyqtSlot()
    def onMapLoad(self):
        self.view.page().runJavaScript("map.setMaxBounds({0})".format(str(self.bbox)))
        self.view.page().runJavaScript("map.fitBounds({0})".format(str(self.bbox)))
        for node in self.graph["nodes"]:
            self.view.page().runJavaScript("L.circle([{0},{1}], 2.0, ".format(float(node["lat"]), float(node["lon"])) + r"{color: 'green'}).addTo(map);")
            for neighbour in node["neighbours"]:
               joins = []
               joins.append([node["lat"],node["lon"]])
               joins.append([neighbour["lat"],neighbour["lon"]])
               self.view.page().runJavaScript("L.polyline({0}, ".format(str(joins)) + r"{color: 'black'}).addTo(map)")
        s = configToString(self.config)
        self.textStorage = s
        self.configTextBox.setPlainText(s)
        # Set numOfPlayerSessions
        page = self.view.page()
        page.runJavaScript("numPlayerSessions = {0};".format(self.config["MAP_ATTRIBUTES"]["num_player_sessions"]))
        ss = r"swal({text: 'Please specifiy the OD pairs for each player session, total OD pairs required = " + "{0}".format(self.config["MAP_ATTRIBUTES"]["num_player_sessions"]) + r"'});"
        page.runJavaScript(r"swal({text: 'Please specifiy the OD pairs for each player session, total OD pairs required = " + "{0}".format(self.config["MAP_ATTRIBUTES"]["num_player_sessions"]) + r"'});")
        print(ss)
    
    @QtCore.pyqtSlot(str, str)
    def updateConfigTextBox(self, newText, traffic_pop_percent):
        print(traffic_pop_percent)
        if (traffic_pop_percent != "-1"):
            self.populationStorage = float(traffic_pop_percent)
            self.traffic_pop_percent -= float(traffic_pop_percent)
            self.trafficPopRemaining.setText("REMAINING TRAFFIC POPULATION % = {0}".format(str(self.traffic_pop_percent)))
        currentText = self.configTextBox.toPlainText()
        self.textStorage = currentText
        currentText += newText
        self.configTextBox.setPlainText(currentText)
        self.config.read_string(currentText)
        with open(self.configPath, 'w') as configFile:
            self.config.write(configFile)

def configToString(config):
    s = "[MAP_ATTRIBUTES]\n"
    for key in config['MAP_ATTRIBUTES']:
        s += (key + " = " + config['MAP_ATTRIBUTES'][key] + "\n")
    s += "\n"
    return s

class GetMapDataWindow(QtWidgets.QWidget):
    def __init__(self):
        super(GetMapDataWindow, self).__init__()
        self.setupUi()
        self.bbox = []
        self.graphJsonPath = os.path.join('utils', 'pathGen', 'Graph', 'nodes.json')
        self.configPath = os.path.join('utils', 'pathGen', 'Graph', 'config.ini')
    
    def append(self, text):
        cursor = self.textOut.textCursor()
        cursor.movePosition(cursor.End)
        cursor.insertText(text)
        self.textOut.ensureCursorVisible()

    def stdoutReady(self):
        text = str(self.process.readAllStandardOutput(), encoding='utf-8')
        print(text.strip())
        self.append(text)

    def stderrReady(self):
        text = str(self.process.readAllStandardError(), encoding='utf-8')
        print(text.strip())
        self.append(text)

    def setupUi(self):
        self.setWindowTitle('Map Generation Tool - [Get Map Data]')
        self.setMinimumSize(1200, 900)
        layout = QtWidgets.QGridLayout()
        layout.setSpacing(10)
        self.setLayout(layout)

        # Map name label
        mapNameLabel = self.mapNameLabel = QtWidgets.QLabel()
        mapNameLabel.setText("MAP NAME: ")
        layout.addWidget(mapNameLabel, 1, 0)

        # Map name textbox
        mapNameTextEdit = self.mapNameTextEdit = QtWidgets.QLineEdit()
        mapNameTextEdit.setText("Enter map name...")
        layout.addWidget(mapNameTextEdit, 1, 1, 1, 2)

        # Lat and Lng label for center of map
        mapCenterLabel = self.mapCenterLabel = QtWidgets.QLabel()
        mapCenterLabel.setText("MAP CENTER ([lat,lng]): ")
        layout.addWidget(mapCenterLabel, 2, 0)

        # Lat and Lng textbox for center of map
        centerLatLngTextEdit = self.centerLatLngTextEdit = QtWidgets.QLineEdit()
        centerLatLngTextEdit.setText("[-31.95053,115.86046]")
        centerLatLngTextEdit.setFixedWidth(175)
        centerLatLngTextEdit.setReadOnly(True)
        layout.addWidget(centerLatLngTextEdit, 2, 1, 1, 2)

        # Extent label
        extentLabel = self.extentLabel = QtWidgets.QLabel()
        extentLabel.setText("EXTENT: ")
        layout.addWidget(extentLabel, 3, 0)

        # Extent textbox
        extentTextBox = self.extentTextBox = QtWidgets.QLineEdit()
        extentTextBox.setText("2")
        layout.addWidget(extentTextBox, 3, 1, 1, 2)

        # Default speed label
        defaultSpeedLabel = self.defaultSpeedLabel = QtWidgets.QLabel()
        defaultSpeedLabel.setText("DEFAULT SPEED (km/h): ")
        layout.addWidget(defaultSpeedLabel, 4, 0)

        # Default speed textbox
        defaultSpeedTextBox = self.defaultSpeedTextBox = QtWidgets.QLineEdit()
        defaultSpeedTextBox.setText("50")
        layout.addWidget(defaultSpeedTextBox, 4, 1, 1, 2)

        # Allow one way roads label
        oneWayRoadsLabel = self.oneWayRoadsLabel = QtWidgets.QLabel()
        oneWayRoadsLabel.setText("ONE WAY ROADS (true/false): ")
        layout.addWidget(oneWayRoadsLabel, 5, 0)

        # Allow one way roads textbox
        oneWayRoadsTextBox = self.oneWayRoadsTextBox = QtWidgets.QLineEdit()
        oneWayRoadsTextBox.setText("true")
        layout.addWidget(oneWayRoadsTextBox, 5, 1, 1, 2)

        # Number of ai cars label
        numAiCarsLabel = self.numAiCarsLabel = QtWidgets.QLabel()
        numAiCarsLabel.setText("# OF AI CARS: ")
        layout.addWidget(numAiCarsLabel, 6, 0)

        # Number of ai cars textbox
        numAiCarsTextBox = self.numAiCarsTextBox = QtWidgets.QLineEdit()
        numAiCarsTextBox.setText("10")
        layout.addWidget(numAiCarsTextBox, 6, 1, 1, 2)

        # Percentage of smart ai cars label
        percentageOfSmartAiCarsLabel = self.percentageOfSmartAiCarsLabel = QtWidgets.QLabel()
        percentageOfSmartAiCarsLabel.setText("% OF SMART AI CARS: ")
        layout.addWidget(percentageOfSmartAiCarsLabel, 7, 0)

        # Percentage of smart ai cars textbox
        percentageOfSmartAiCarsTextBox = self.percentageOfSmartAiCarsTextBox = QtWidgets.QLineEdit()
        percentageOfSmartAiCarsTextBox.setText("30")
        layout.addWidget(percentageOfSmartAiCarsTextBox, 7, 1, 1, 2)

        # Sepcifies the number of player sessions label
        numOfPlayerSessionsLabel = self.numOfPlayerSessionsLabel = QtWidgets.QLabel()
        numOfPlayerSessionsLabel.setText("# OF PLAYER SESSIONS: ")
        layout.addWidget(numOfPlayerSessionsLabel, 8, 0)

        # Specifies the number of player sessions textbox
        numOfPlayerSessionsTextBox = self.numOfPlayerSessionsTextBox = QtWidgets.QLineEdit()
        numOfPlayerSessionsTextBox.setText("3")
        layout.addWidget(numOfPlayerSessionsTextBox, 8, 1, 1, 2)

        # Refresh map visual box button
        refreshMapBtn = self.refreshMapBtn = QtWidgets.QPushButton("Refresh")
        refreshMap = functools.partial(self.refreshMap)
        refreshMapBtn.clicked.connect(refreshMap)
        layout.addWidget(refreshMapBtn, 9, 0, 1, 3)

        # Sets up the OSM html / javascript map as a widget
        view = self.view = QtWebEngineWidgets.QWebEngineView()
        channel = self.channel = QtWebChannel.QWebChannel()
        channel.registerObject("GetMapDataWindow", self)
        view.page().setWebChannel(channel)
        file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "src/main/assets/map.html",
        )
        self.view.setUrl(QtCore.QUrl.fromLocalFile(file))
        layout.addWidget(view, 1, 3, 30, 30)

        # Pan to lat and lon on map label
        panToLabel = self.panToLabel = QtWidgets.QLabel()
        panToLabel.setText("PAN TO ([lat,lng]):")
        layout.addWidget(panToLabel, layout.rowCount()-3, 0, 1, 2)

        # Pan to lat and lon on map textbox
        panToTextBox = self.panToTextBox = QtWidgets.QLineEdit()
        panToTextBox.setText("[-31.95053, 115.86046]")
        layout.addWidget(panToTextBox, layout.rowCount()-2, 0, 1, 2)

        # Pan to lat and lon on map button
        panToBtn = self.panToBtn = QtWidgets.QPushButton("Go To Coord!") #
        panMapToLatLng = functools.partial(self.panMapToLatLng)
        panToBtn.clicked.connect(panMapToLatLng)
        layout.addWidget(panToBtn, layout.rowCount()-2, 2, 1, 1)

        # Get map data button
        getMapDataBtn = self.getMapDataBtn = QtWidgets.QPushButton("Get Map Data!")
        getData = functools.partial(self.getData)
        getMapDataBtn.clicked.connect(getData)
        layout.addWidget(getMapDataBtn, layout.rowCount()-1, 0, 1, 3)

        # Console textbox
        textOut = self.textOut = QtWidgets.QTextEdit()
        textOut.setFixedHeight(100)
        textOut.setReadOnly(True)
        layout.addWidget(textOut, layout.rowCount(), 0, 1, layout.columnCount())
    
    @QtCore.pyqtSlot(float, float)
    def onMapMove(self, lat, lng):
        self.centerLatLngTextEdit.setText("[{0:.5f},{1:.5f}]".format(lat, lng))
        self.refreshMap()

    def refreshMap(self):
        global latLng
        page = self.view.page()
        latLng = self.centerLatLngTextEdit.text().strip("[").strip("]").split(",")
        boundary = hw.getBox(float(latLng[0]), float(latLng[1]), 16, int(self.extentTextBox.text()))
        x = []
        for item in boundary:
            x.extend(item)
        self.bbox = [[x[0],x[1]],[x[2],x[3]]]
        page.runJavaScript(r"if (bbox != null) {map.removeLayer(bbox)}")
        page.runJavaScript("bbox = L.rectangle([[{0},{1}],[{2},{3}]]).addTo(map);".format(x[0],x[1],x[2],x[3]))

    def panMapToLatLng(self):
        global latLng
        page = self.view.page()
        newLatLng = self.panToTextBox.text()
        page.runJavaScript("map.flyTo({0})".format(newLatLng))
        self.centerLatLngTextEdit.setText(newLatLng)
        latLng = newLatLng

    def getData(self):
        self.process = QtCore.QProcess(self)
        self.process.readyReadStandardOutput.connect(self.stdoutReady)
        self.process.readyReadStandardError.connect(self.stderrReady)
        self.process.started.connect(lambda: self.disableUi())
        self.process.finished.connect(lambda: self.goToODPairWindow())
        toJsonPath = os.path.join('utils', 'toJSON.py')
        outputDestination = os.path.join('utils', 'pathGen', 'Graph')
        if sys.platform.startswith('win32'):
            self.process.start(os.path.join("windows-env", "python.exe"), [toJsonPath, self.extentTextBox.text(), str(latLng[0]), str(latLng[1]), "16", self.mapNameTextEdit.text(), outputDestination, self.defaultSpeedTextBox.text(), self.oneWayRoadsTextBox.text()])
        elif sys.platform.startswith('darwin'):
            self.process.start('mac-env/bin/python', [toJsonPath, self.extentTextBox.text(), str(latLng[0]), str(latLng[1]), "16", self.mapNameTextEdit.text(), outputDestination, self.defaultSpeedTextBox.text(), self.oneWayRoadsTextBox.text()])
        elif sys.platform.startswith('linux'):
            self.process.start('linux-env/bin/python', [toJsonPath, self.extentTextBox.text(), str(latLng[0]), str(latLng[1]), "16", self.mapNameTextEdit.text(), outputDestination, self.defaultSpeedTextBox.text(), self.oneWayRoadsTextBox.text()])

    def goToODPairWindow(self):
        print("[INFO] - Graph representation written to file.")
        print("[INFO] - Parsing graph representation and config file, please wait...")
        graph = self.parseGraphJson()
        config = self.writeConfig()
        print("[INFO] - Config file created.")
        od = OriginDestinationWindow(config, graph, self.bbox)
        od.showMaximized()
        gmd.close()

    def parseGraphJson(self):
        with open(self.graphJsonPath) as f:
            graph = json.load(f)
            return graph

    def writeConfig(self):
        config = configparser.ConfigParser()
        config.add_section('MAP_ATTRIBUTES')
        config.set('MAP_ATTRIBUTES', 'name', self.mapNameTextEdit.text())
        config.set('MAP_ATTRIBUTES', 'output_dest', '../wwwroot/webgl/output')
        config.set('MAP_ATTRIBUTES', 'num_player_sessions', self.numOfPlayerSessionsTextBox.text())
        config.set('MAP_ATTRIBUTES', 'num_ai_Cars', self.numAiCarsTextBox.text())
        config.set('MAP_ATTRIBUTES', 'traffic_percentage_smart', self.percentageOfSmartAiCarsTextBox.text())
        config.set('MAP_ATTRIBUTES', 'latitude', self.centerLatLngTextEdit.text().split(',')[0].strip(']').strip('['))
        config.set('MAP_ATTRIBUTES', 'longitude', self.centerLatLngTextEdit.text().split(',')[1].strip(']').strip('['))
        config.set('MAP_ATTRIBUTES', 'extent', self.extentTextBox.text())
        config.set('MAP_ATTRIBUTES', 'zoom', '16')
        config.set('MAP_ATTRIBUTES', 'default_speed', self.defaultSpeedTextBox.text())
        config.set('MAP_ATTRIBUTES', 'allow_one_way', self.oneWayRoadsTextBox.text())
        with open(self.configPath, 'w') as configFile:
            config.write(configFile)
        return config

    def disableUi(self):
        self.mapNameTextEdit.setEnabled(False)
        self.numOfPlayerSessionsTextBox.setEnabled(False)
        self.centerLatLngTextEdit.setEnabled(False)
        self.extentTextBox.setEnabled(False)
        self.defaultSpeedTextBox.setEnabled(False)
        self.oneWayRoadsTextBox.setEnabled(False)
        self.numAiCarsTextBox.setEnabled(False)
        self.percentageOfSmartAiCarsTextBox.setEnabled(False)
        self.panToTextBox.setEnabled(False)
        self.view.setEnabled(False)
        self.refreshMapBtn.setEnabled(False)
        self.panToBtn.setEnabled(False)
        self.getMapDataBtn.setEnabled(False)
        page = self.view.page()
        page.runJavaScript(r"swal({text: 'Getting OpenStreetMaps data... Progress will be displayed in the console below.', showConfirmButton: false});")


if __name__ == "__main__":
    global latLng
    latLng = [-31.95053, 115.86046]
    app = QtWidgets.QApplication(sys.argv)
    gmd = GetMapDataWindow()
    gmd.showMaximized()
    sys.exit(app.exec_())
