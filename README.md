# Map Generation Tool

Interactive GUI for the client to prepare a map and the associated AI car path generations prior to loading into the Unity game.

## How to Run 'out-of-the-box'
- Windows: Run 'Windows Map Generator Tool.bat'
- MacOS: Run 'MacOS Map Generator Tool'
- Linux: Run 'Linux Map Generator Tool.sh'

## How to Setup and Run (Programmatically)

1. Install Anaconda / Miniconda (<https://docs.anaconda.com/anaconda/install/>)

2. cd into the root directory

    ```terminal
    conda env create --file environment.yaml
    ```

3. Activate your Conda environment

    ```terminal
    conda activate gta
    ```

4. Run the interactive GUI using the following code snippet:

    ```terminal
    python main.py
    ```

5. This tool will output a directory with the name Resources/Maps/{Map Name}

## For Developers

When any changes are made to the path generation C# script (Program.cs) located in the "~/maptool-pathing/utils/pathGen/Pathing" directory ... the .DLL must be rebuilt before starting the interactive GUI tool.

1. To rebuild the .DLL, cd into the "~/maptool-pathing/utils/pathGen" directory, and run the following code snippet:

    ```terminal
    python pathGen.py --rebuild
    ```

2. If any Nuget packages could not be found on your system, simply open the "Pathing.sln" file in Visual Studio and restore the Nuget packages for the project. Specific instructions on how to do this are beyond scope.

3. If the .DLL is rebuilt successfully, then you can cd back into the root directory of this repo and start the interactive GUI by running the following code snippet:

    ```terminal
    python main.py
    ```
