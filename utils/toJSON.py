import json
from overpy import Overpass
import highways as hw
import pygeohash as gh
from geopy.distance import geodesic
import configparser
import argparse
import sys
import os

def get_node_by_id(nodes, node_id):
    for node in nodes:
        if node["nodeId"] == node_id:
            return node
    return None

# Build a dictionary of the form:
# Note the distance is in km and the time is in h
"""
    ways: [
        {
            {wayId}
            {name}
            {highwayType}
            {maxSpeed}
        }
    ],
    nodes: [
        {
            {nodeId}
            {lat}
            {lon}
            {wayID}
            {isStart}
            {isEnd}
            neighbours: [
                {
                    {neighbourId}
                    {distance}
                    {time}
                    {isOneWay}
                    {wayId}
                    {isBlocked}
                }
            ]
        }
    ]
"""

# Map metadata
NAME = "Test"
OUTPUT_DEST = "../"
DEFAULT_SPEED = 50.0
ALLOW_ONE_WAY = True

# Get nodes from Overpass
api = Overpass()
EXTENT = 2
LAT = -31.950527
LON = 115.860458
ZOOM = 16

def getData(extent=EXTENT, lat=LAT, lon=LON, zoom=ZOOM, name=NAME, output_dest=OUTPUT_DEST, default_speed=DEFAULT_SPEED, allow_one_way=ALLOW_ONE_WAY):
    fullDict = {
    "ways": [],
    "nodes": []
    }
    DRAW_WAYS = [
    'motorway', 'motorway_link',
    'trunk', 'trunk_link',
    'primary','primary_link',
    'secondary', 'secondary_link',
    'tertiary',
    'unclassified',
    'residential']
    nodesSeen = set()

    print("[INFO] - File output locations => '~/maptool-pathing/utils/pathGen/Graph/nodes.json' and '~/maptool-pathing/utils/pathGen/Graph/config.ini'")
    print("[INFO] - Querying overpass api, please wait...")
    sys.stdout.flush()
    result = hw.getWays(lat,lon,zoom,extent)
    print("[INFO] - Data retrieved!")
    sys.stdout.flush()
    print("[INFO] - Constructing graph representation, please wait...")
    sys.stdout.flush()
    for way in result:
        if way.tags.get("highway", "n/a") in DRAW_WAYS:
            way_speed = way.tags.get("maxspeed", "50")
            if isinstance(way_speed, str):
                if ";" in way_speed:
                    way_speed_split = way_speed.split(";")
                    way_speed = float(way_speed_split[0])
                else:
                    way_speed_split = way_speed.split(" ")
                    way_speed = float(way_speed_split[0])
                    if len(way_speed_split) > 1 and way_speed_split[1] == 'mph':
                        way_speed = way_speed * 1.60934
            # Get relevant information about way
            wayDict = {
                "wayId": str(way.id),
                "name": way.tags.get("name", "n/a"),
                "highwayType": way.tags.get("highway", "n/a"),
                "oneWay": (way.tags.get("oneway", "no") == "yes") or (way.tags.get("junction", "n/a") == "roundabout"),
                "maxSpeed": way_speed
            }
            fullDict["ways"].append(wayDict)
            for i in range(0, len(way.nodes)):
                node = way.nodes[i]
                geohash = gh.encode(node.lat, node.lon, precision=11)
                # Check if node already exists and add it all if not
                if geohash not in nodesSeen:
                    nodeHighwayType = node.tags.get("highway", "n/a")
                    nodeDict = {
                        "nodeId": str(geohash),
                        "lat": str(node.lat),
                        "lon": str(node.lon),
                        "wayId": str(way.id),
                        "parents": 0,
                        "isSignal": nodeHighwayType=="traffic_signals",
                        "isStart": i == 0,
                        "isEnd": i == len(way.nodes) - 1,
                        "neighbours": []
                    }
                    fullDict["nodes"].append(nodeDict)
                    nodesSeen.add(geohash)
                # Add neighbours
                # Always add one after if exists
                if i < len(way.nodes) - 1:
                    neighbour_lat = way.nodes[i+1].lat
                    neighbour_lon = way.nodes[i+1].lon
                    neighbour_geohash = gh.encode(neighbour_lat, neighbour_lon, precision=11)
                    distance = geodesic((node.lat, node.lon), (neighbour_lat, neighbour_lon)).km
                    neighbourDict = {
                        "neighbourId": str(neighbour_geohash),
                        "lat": str(neighbour_lat),
                        "lon": str(neighbour_lon),
                        "distance": str(distance),
                        "time": str(distance / way_speed),
                        "isOneWay": wayDict['oneWay'],
                        "wayId": str(way.id),
                        "isBlocked": False
                    }
                    nodePointer = get_node_by_id(fullDict["nodes"], geohash)
                    nodePointer["neighbours"].append(neighbourDict)
                # Only add one before if exists AND is not oneway
                if i > 0 and not wayDict['oneWay']:
                    neighbour_lat = way.nodes[i - 1].lat
                    neighbour_lon = way.nodes[i - 1].lon
                    neighbour_geohash = gh.encode(neighbour_lat, neighbour_lon, precision=11)
                    distance = geodesic((node.lat, node.lon), (neighbour_lat, neighbour_lon)).km
                    neighbourDict = {
                        "neighbourId": str(neighbour_geohash),
                        "lat": str(neighbour_lat),
                        "lon": str(neighbour_lon),
                        "distance": str(distance),
                        "time": str(distance / way_speed),
                        "isOneWay": wayDict['oneWay'],
                        "wayId": str(way.id),
                        "isBlocked": False
                    }
                    nodePointer = get_node_by_id(fullDict["nodes"], geohash)
                    nodePointer["neighbours"].append(neighbourDict)

    for node in fullDict['nodes']:
        for child in node['neighbours']:
            nodePointer = get_node_by_id(fullDict["nodes"], child['neighbourId'])
            nodePointer['parents'] += 1
    print("[INFO] - Complete!")
    sys.stdout.flush()


    # Convert to JSON and output to a file
    with open(os.path.join(output_dest, 'nodes.json'), 'w') as fp:
        json.dump(fullDict, fp)

    # Export map attributes to .ini
        map_config = configparser.ConfigParser()
        map_config["MAP_ATTRIBUTES"] = {
            'Name': name,
            'OutputDest': os.path.join(output_dest, 'nodes.json'),
            'Latitude': lat,
            'Longitude': lon,
            'Extent': extent,
            'Zoom': zoom,
            'DefaultSpeed': default_speed,
            'AllowOneWay': allow_one_way
        }
        with open(os.path.join(output_dest, 'config.ini'), 'w') as configfile:
            map_config.write(configfile)

if __name__ == "__main__":
    try:
        # Check if script is run with arguments
        if (len(sys.argv) == 1):
            getData()
        elif (len(sys.argv) == 9):
            getData(
                extent=int(sys.argv[1]), 
                lat=float(sys.argv[2]), 
                lon=float(sys.argv[3]), 
                zoom=int(sys.argv[4]), 
                name=sys.argv[5], 
                output_dest=sys.argv[6], 
                default_speed=float(sys.argv[7]), 
                allow_one_way=bool(sys.argv[8]))
        else:
            print("[ERROR] - Invalid input to toJSON.py")       
    except:
        print("[ERROR] - Unexpected error:", sys.exc_info())