﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using IniParser;
using IniParser.Model;
using System.Threading.Tasks;
using System.Linq;
namespace Pathing
{
    class Program
    {
        static void Main(string[] args)
        {

            // Read in our .JSON file containing road information
            Graph graph = new Graph();
            if (args.Length < 1) {
                graph.ReadInGraphFromJSON(@"/Users/Daniel/OneDrive - The University of Western Australia/Uni/Sem 2 2020/maptool-pathing/DefaultMap2/nodes.json"); // Uses this path when running pathGen.py python script
            } else {
                graph.ReadInGraphFromJSON(args[0].ToString()); // Uses command line argument string for nodes.json path
            }
            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------START-----------------------------------------------");
            Console.WriteLine("[INFO] - Number of graph nodes: " + graph.nodes.Count.ToString());

            //Read OD info from config.ini
            List<(double, double, double)> spawnAreas = new List<(double, double, double)>();
            var parser = new FileIniDataParser();
            IniData data;
            if (args.Length < 1) {
                data = parser.ReadFile(@"/Users/Daniel/OneDrive - The University of Western Australia/Uni/Sem 2 2020/maptool-pathing/DefaultMap2/config.ini"); // Uses this path when running pathGen.py python script
            } else {
                data = parser.ReadFile(args[1].ToString()); // Uses command line argument string for config.ini path
            }
            IniParser.Model.SectionDataCollection sections = data.Sections;
            List<ODPair> spawns = new List<ODPair>();
            JsonSerializer serializer = new JsonSerializer();
            var mapName = data["MAP_ATTRIBUTES"]["name"];
            foreach (IniParser.Model.SectionData section in sections)
            {
                // Handle all the player session OD pairs from config file
                if (section.SectionName.Contains("PLAYER_SESSION")) {
                    string originString = data[section.SectionName]["origin"];
                    string[] parts = originString.Split(',');
                    double originLat = Convert.ToDouble(parts[0].Replace("[", ""));
                    double originLon = Convert.ToDouble(parts[1].Replace("]", ""));

                    string destinationString = data[section.SectionName]["destination"];
                    parts = destinationString.Split(',');
                    double destinationLat = Convert.ToDouble(parts[0].Replace("[", ""));
                    double destinationLon = Convert.ToDouble(parts[1].Replace("]", ""));

                    string originNodeID = getClosestNode((originLat, originLon), graph);
                    string destinationNodeID = getClosestNode((destinationLat, destinationLon), graph);

                    PlayerODPair playerODPair = new PlayerODPair(originNode: originNodeID, destinationNode: destinationNodeID);

                    // Dump session player origin and destination node to .json
                    Directory.CreateDirectory(Path.Combine(mapName, "playerPaths"));
                    var playerPathsOutputPath = Path.Combine(mapName, "playerPaths", "session" + section.SectionName.Substring(section.SectionName.Length-1) + ".json");
                    using (StreamWriter file = File.CreateText(playerPathsOutputPath)) {
                        using (JsonWriter writer = new JsonTextWriter(file))
                        {
                            serializer.Formatting = Formatting.Indented;
                            serializer.NullValueHandling = NullValueHandling.Ignore;
                            serializer.Serialize(file, playerODPair);
                        }
                    }
                }

                // Handle all the traffic OD pairs from config file
                if (section.SectionName.Contains("OD_PAIR"))
                {
                    string originString = data[section.SectionName]["origin"];
                    string[] parts = originString.Split(',');
                    double originLat = Convert.ToDouble(parts[0].Replace("[", ""));
                    double originLon = Convert.ToDouble(parts[1].Replace("]", ""));

                    string destinationString = data[section.SectionName]["destination"];
                    parts = destinationString.Split(',');
                    double destinationLat = Convert.ToDouble(parts[0].Replace("[", ""));
                    double destinationLon = Convert.ToDouble(parts[1].Replace("]", ""));

                    double spawnRadius = Convert.ToDouble(data[section.SectionName]["spawn_radius"]);
                    double popPercentage = Convert.ToDouble(data[section.SectionName]["traffic_pop_percent"]);

                    List<string> nodesInRadius = new List<string>();
                    foreach (Graph.Node node in graph.nodes)
                    {
                        double dist = getDistanceBetweenCoords((originLat, originLon), (node.lat, node.lon));
                        if (dist<=spawnRadius)
                        {
                            nodesInRadius.Add(node.nodeId);
                        }
                    }
                    string destinationNodeID = getClosestNode((destinationLat, destinationLon), graph);

                    spawns.Add(new ODPair(section.SectionName, nodesInRadius, popPercentage, destinationNodeID));
                }
            }

            Console.WriteLine("[INFO] - Matching spawn nodes to graph nodes and calculating paths...");
            Console.WriteLine("------------------------------------------------------------------------------------------------");

            Random rand = new Random();
            int totalNumCars = Convert.ToInt32(data["MAP_ATTRIBUTES"]["num_ai_cars"]);
            double proportionSmart = Convert.ToDouble(data["MAP_ATTRIBUTES"]["traffic_percentage_smart"])/100;
            int numCarsDumb = Convert.ToInt32(totalNumCars * (1-proportionSmart));
            int numCarsSmart = Convert.ToInt32(totalNumCars * (proportionSmart));
            Console.WriteLine($"Total: {totalNumCars}\nDumb: {numCarsDumb}");
            int counterDumb = 0;
            int counterSmart = 0;
            List<AICar> aiCars = new List<AICar>();
            List<SmartAICar> smartAICars = new List<SmartAICar>();
            double percentageRemaining = 100;
            var timer = System.Diagnostics.Stopwatch.StartNew();
            DumbTraffic dumbTraffic = new DumbTraffic(graph);
            BaseTraffic baseTraffic = new BaseTraffic(graph);
            int shortestPathCounter = 0;

            foreach (ODPair pair in spawns)
            {
                int carsToSpawnDumb = Convert.ToInt32((pair.percentPop/100) * numCarsDumb);
                int carsToSpawnSmart = Convert.ToInt32((pair.percentPop/100) * numCarsSmart);
                List<int> pairIDsDumb = new List<int>();
                for(int i=counterDumb; i<(counterDumb+carsToSpawnDumb); i++)
                {
                    pairIDsDumb.Add(i);
                }
                counterDumb += carsToSpawnDumb;
                List<int> pairIDsSmart = new List<int>();
                for(int i=counterSmart; i<(counterSmart+carsToSpawnSmart); i++)
                {
                    pairIDsSmart.Add(i);
                }
                counterSmart += carsToSpawnSmart;
                
                Parallel.ForEach(pairIDsDumb, id =>
                {
                    string spawnID = pair.validSpawns[rand.Next(pair.validSpawns.Count)];

                    //Uncomment this to use Yen's algorithm for K-shortest paths
                    //List<List<string>> pathChoices = dumbTraffic.PathToFrom(spawnID, pair.destinationNode, 3);
                    //List<string> path = pathChoices[rand.Next(pathChoices.Count)];

                    //This uses Djikstra's algorithm with map variation
                    List<string> path = dumbTraffic.PathToFrom(spawnID, pair.destinationNode);
                    List<string> shortestPath = baseTraffic.PathToFrom(spawnID, pair.destinationNode);
                    if (PathsAreEqual(path, shortestPath)) shortestPathCounter += 1;
                    AICar car = new AICar(id, spawnID, pair.destinationNode, path);
                    aiCars.Add(car);
                    Console.WriteLine($"[LOG] - Path {id.ToString()} calculated...");
                });

                Parallel.ForEach(pairIDsSmart, id =>
                {
                    string spawnID = pair.validSpawns[rand.Next(pair.validSpawns.Count)];
                    SmartAICar car = new SmartAICar(id, spawnID, pair.destinationNode);
                    smartAICars.Add(car);
                    Console.WriteLine($"[LOG] - Smart Start/End {id.ToString()} calculated...");
                });

                percentageRemaining -= pair.percentPop;
                Console.WriteLine("------------------------------------------------------------------------------------------------");
                Console.WriteLine($"[INFO] - Spawned {pair.percentPop}% for {pair.pairName}");
                Console.WriteLine("------------------------------------------------------------------------------------------------");
            }

            if (percentageRemaining>0)
            {
                int carsToSpawnDumb = Convert.ToInt32((percentageRemaining / 100) * numCarsDumb);
                int carsToSpawnSmart = Convert.ToInt32((percentageRemaining / 100) * numCarsSmart);
                List<int> pairIDsDumb = new List<int>();
                for (int i = counterDumb; i < (counterDumb + carsToSpawnDumb); i++)
                {
                    pairIDsDumb.Add(i);
                }
                counterDumb += carsToSpawnDumb;
                List<int> pairIDsSmart = new List<int>();
                for (int i = counterSmart; i<(counterSmart + carsToSpawnSmart); i++)
                {
                    pairIDsSmart.Add(i);
                }
                counterSmart += carsToSpawnSmart;
                Parallel.ForEach(pairIDsDumb, id =>
                {
                    string spawnID = graph.nodes[rand.Next(graph.nodes.Count)].nodeId;
                    string destinationID = graph.nodes[rand.Next(graph.nodes.Count)].nodeId;

                    //Uncomment this to use Yen's algorithm for K-shortest paths
                    //List<List<string>> pathChoices = dumbTraffic.PathToFrom(spawnID, destinationID, 3);
                    //List<string> path = pathChoices[rand.Next(pathChoices.Count)];

                    //This uses Djikstra's algorithm with map variation
                    List<string> path = dumbTraffic.PathToFrom(spawnID, destinationID);
                    List<string> shortestPath = baseTraffic.PathToFrom(spawnID, destinationID);
                    if (PathsAreEqual(path, shortestPath)) shortestPathCounter += 1;
                    AICar car = new AICar(id, spawnID, destinationID, path);
                    aiCars.Add(car);
                    counterDumb += 1;
                    Console.WriteLine($"[LOG] - Path {id.ToString()} calculated...");
                });
                
                Parallel.ForEach(pairIDsSmart, id =>
                {
                    string spawnID = graph.nodes[rand.Next(graph.nodes.Count)].nodeId;
                    string destinationID = graph.nodes[rand.Next(graph.nodes.Count)].nodeId;

                    SmartAICar car = new SmartAICar(id, spawnID, destinationID);
                    smartAICars.Add(car);
                    counterSmart += 1;
                    Console.WriteLine($"[LOG] - Smart Start/End {id.ToString()} calculated...");
                });
                Console.WriteLine("------------------------------------------------------------------------------------------------");
                Console.WriteLine($"[INFO] - Spawned {percentageRemaining}% for random origin and destination pairs.");
                percentageRemaining = 0;
            }

            // Get all of the fields from config.ini that need to go into the output.json file
            var output_dest = data["MAP_ATTRIBUTES"]["output_dest"];
            var latitude = data["MAP_ATTRIBUTES"]["latitude"];
            var longitude = data["MAP_ATTRIBUTES"]["longitude"];
            var extent = data["MAP_ATTRIBUTES"]["extent"];
            var zoom = data["MAP_ATTRIBUTES"]["zoom"];
            var default_speed = data["MAP_ATTRIBUTES"]["default_speed"];
            var allow_one_way = data["MAP_ATTRIBUTES"]["allow_one_way"];
            var outputFileObject = new outputFile(mapName, output_dest, double.Parse(latitude), double.Parse(longitude), int.Parse(extent), int.Parse(zoom), int.Parse(default_speed), bool.Parse(allow_one_way));


            // Dump car path data to .json file
            Directory.CreateDirectory(mapName);
            var pathingOutputPath = Path.Combine(mapName, "paths.json");
            var smartPairsOutputPath = Path.Combine(mapName, "smartODPairs.json");
            var configOutputPath = Path.Combine(mapName, "config.ini");
            var graphOutputPath = Path.Combine(mapName, "nodes.json");
            var outputFileJsonPath = Path.Combine(mapName, "output.json");
            using (StreamWriter file = File.CreateText(graphOutputPath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Formatting = Formatting.Indented;
                    serializer.NullValueHandling = NullValueHandling.Ignore;
                    serializer.Serialize(file, graph);
                }
            using (StreamWriter file = File.CreateText(pathingOutputPath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Formatting = Formatting.Indented;
                    serializer.NullValueHandling = NullValueHandling.Ignore;
                    serializer.Serialize(file, new aiPaths(aiCars));
                }
            using (StreamWriter file = File.CreateText(smartPairsOutputPath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Formatting = Formatting.Indented;
                    serializer.NullValueHandling = NullValueHandling.Ignore;
                    serializer.Serialize(file, new smartCars(smartAICars));
                }
            using (StreamWriter file = File.CreateText(outputFileJsonPath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Formatting = Formatting.Indented;
                    serializer.NullValueHandling = NullValueHandling.Ignore;
                    serializer.Serialize(file, outputFileObject);
                }
            using (StreamWriter file = File.CreateText(configOutputPath))
                file.WriteLine(data.ToString());
            

            
            // Print total time taken
            timer.Stop();
            Console.WriteLine("------------------------------------------DONE------------------------------------------------");
            Console.WriteLine("Total time taken: " + timer.Elapsed.TotalSeconds.ToString());
            Console.WriteLine("Average time per car calculation: " + (timer.Elapsed.TotalSeconds / (double)aiCars.Count));
            Console.WriteLine($"Graph representation output: {graphOutputPath}");
            Console.WriteLine($"Map config output: {configOutputPath}");
            Console.WriteLine($"AI car path(s) output: {pathingOutputPath}");
            Console.WriteLine($"Smart car start/end points output: {smartPairsOutputPath}");
            Console.WriteLine($"Output.json (required by unity) output: {outputFileJsonPath}");
            Console.WriteLine("------------------------------------------------------------------------------------------------");
        }

        public static double getDistanceBetweenCoords((double, double) pointA, (double, double) pointB)
        {
            var d1 = pointA.Item1 * (Math.PI / 180.0);
            var x = pointA.Item2 * (Math.PI / 180.0);
            var d2 = pointB.Item1 * (Math.PI / 180.0);
            var y = pointB.Item2 * (Math.PI / 180.0) - x;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(y / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }

        public static string getClosestNode((double, double) point, Graph graph)
        {
            Graph.Node closestNode = graph.nodes[0];
            double minDist = getDistanceBetweenCoords(point, (closestNode.lat, closestNode.lon));
            foreach (Graph.Node node in graph.nodes)
            {
                double dist = getDistanceBetweenCoords(point, (node.lat, node.lon));
                if (dist<minDist)
                {
                    closestNode = node;
                    minDist = dist;
                }
            }
            return closestNode.nodeId;
        }

        public static bool PathsAreEqual(List<string> pathA, List<string> pathB)
        {
            if (pathA.Count != pathB.Count) return false;
            for(int i=0; i<pathA.Count; i++)
            {
                if (!(pathA[i].Equals(pathB[i]))) return false;
            }
            return true;
        }
    }
            // {
            //     "name": "MapDeserTest",
            //     "output_dest": "../wwwroot/webgl/output",
            //     "latitude": -31.93144,
            //     "longitude": 115.85662,
            //     "extent": 2,
            //     "zoom": 16,
            //     "default_speed": 50,
            //     "allow_one_way": true
            // }
    public class outputFile {
        public String name {get; set;}
        public String output_dest {get; set;}
        public double latitude {get; set;}
        public double longitude {get; set;}
        public int extent {get; set;}
        public int zoom {get; set;}
        public int default_speed {get; set;}
        public bool allow_one_way {get; set;}

        public outputFile(String name, String output_dest, double latitude, double longitude, int extent, int zoom, int default_speed, bool allow_one_way) {
            this.name = name;
            this.output_dest = output_dest;
            this.latitude = latitude;
            this.longitude = longitude;
            this.extent = extent;
            this.zoom = zoom;
            this.default_speed = default_speed;
            this.allow_one_way = allow_one_way;
        }
    }

    public class aiPaths {
        public List<AICar> cars {get; set;}

        public aiPaths(List<AICar> aiCars) {
            this.cars = aiCars;
        }
    }

    public class smartCars {
        public List<SmartAICar> cars {get; set;}
        public smartCars(List<SmartAICar> aiCars)
        {
            this.cars = aiCars;
        }
    }

    public class AICar
    {
        public int id { get; set; }
        public string startNode { get; set; }
        public string destinationNode { get; set; }
        public List<string> path { get; set; }

        public AICar(int id, string startNode, string destinationNode, List<string> path)
        {
            this.id = id;
            this.startNode = startNode;
            this.destinationNode = destinationNode;
            this.path = path;
        }
    }

    public class SmartAICar
    {
        public int id { get; set; }
        public string startNode { get; set; }
        public string destinationID { get; set; }

        public SmartAICar(int id, string startNode, string destinationNode)
        {
            this.id = id;
            this.startNode = startNode;
            this.destinationID = destinationNode;
        }
    }

    public readonly struct ODPair
    {
        public string pairName { get; }
        public List<string> validSpawns { get; }
        public double percentPop { get; }
        public string destinationNode { get; }

        public ODPair(string pairName, List<string> validSpawns, double percentPop, string destinationNode)
        {
            this.pairName = pairName;
            this.validSpawns = validSpawns;
            this.percentPop = percentPop;
            this.destinationNode = destinationNode;
        }
    }

    public readonly struct PlayerODPair
    {
        public string originNode { get; }
        public string destinationNode { get; }

        public PlayerODPair(string originNode, string destinationNode)
        {
            this.originNode = originNode;
            this.destinationNode = destinationNode;
        }
    }
}