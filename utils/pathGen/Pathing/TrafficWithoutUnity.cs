﻿//using Newtonsoft.Json;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Pathing
//{
//    /// <summary>
//    ///     A class that represents an individual traffic entity within the game.
//    /// </summary>
//    class TrafficWithoutUnity
//    {
//        // TRAFFIC PROPERTIES
//        public Graph.Node origin { get; set; }
//        public Graph.Node destination { get; set; }
//        public Graph.Node currentNode { get; set; }
//        public Graph.Node nextNode { get; set; }

//        //stores x, y coordinates of the intermediate steps between two nodes
//        public Queue<(float, float)> intermediatePositions;
//        public int Velocity { get; set; }
//        public Graph parentGraph { get; }
//        public List<string> path { get; set; }

//        /// <summary>
//        ///     TrafficWithoutUnity() class constructor.
//        /// </summary>
//        /// <param name="originNode"> The traffic entities origin node, type Graph.Node.</param>
//        /// <param name="destinationNode"> The traffic entities destination node, type Graph.Node.</param>
//        /// <param name="g"> The road network graph (<see cref="Graph"/>), type Graph.</param>
//        public TrafficWithoutUnity(Graph.Node originNode, Graph.Node destinationNode, Graph g)
//        {
//            this.origin = originNode;
//            this.destination = destinationNode;
//            this.currentNode = originNode;
//            this.intermediatePositions = new Queue<(float, float)>();
//            this.parentGraph = g;
//            this.path = parentGraph.PathToFrom(originNode.nodeId, destinationNode.nodeId);

//            //If there is more than one node in the path, set the initial nextNode as the second node in the path
//            this.nextNode = (path.Count > 1) ? parentGraph.nodeIdNodeDict[path[1]] : null;
//            // Constant velocity for all traffic, change with unity integration
//            this.Velocity = 2;

//            //Immediately initialise the first intermediatePositions queue
//            this.ToNode();
//        }

//        /// <summary>
//        ///     Populates intermediate positions Queue with the steps vehicles should take
//        ///     on the way to the next node. Number of steps is determined by the distance between nodes
//        ///     and the traffic entities velocity.
//        /// </summary>
//        public void ToNode()
//        {
//            //If the path is finished (next node is null), set the intermediatePositions queue to null to reflect this
//            if (nextNode==null)
//            {
//                intermediatePositions = null;
//                return;
//            }

//            // Pythagoras' theorem
//            double vectorLength = Math.Sqrt(Math.Pow(nextNode.lat - currentNode.lat, 2) + Math.Pow(nextNode.lon - currentNode.lon, 2));
//            int steps = (int)vectorLength / this.Velocity;
            
//            //If the distance is too short to take intermediate steps between
//            if (steps == 0)
//            {
//                //if the next node is the same as the current node, use this position
//                if (nextNode.lat == currentNode.lat && nextNode.lon == currentNode.lon)
//                {
//                    intermediatePositions.Enqueue(((float)currentNode.lat, (float)currentNode.lon));
//                }
//                else
//                //else just use the two nodes as the only steps
//                {
//                    intermediatePositions.Enqueue(((float)currentNode.lat, (float)currentNode.lon));
//                    intermediatePositions.Enqueue(((float)nextNode.lat, (float)nextNode.lon));
//                }
//            }
//            else
//            {
//                //Calculate the horizontal and vertical distances to step between the nodes each tick
//                double xStep = (nextNode.lat - currentNode.lat) / steps;
//                double yStep = (nextNode.lon - currentNode.lon) / steps;

//                //Enqueue the coordinates of each step to be taken
//                for (int i = 0; i < steps; i++)
//                {
//                    float newX = (float)(currentNode.lat + (i * xStep));
//                    float newY = (float)(currentNode.lon + (i * yStep));
//                    intermediatePositions.Enqueue((newX, newY));
//                }
//                //Enqueue the next node as the final position
//                intermediatePositions.Enqueue(((float)nextNode.lat, (float)nextNode.lon));
//                //reverse the queue so that is progresses from start -> finish
//                intermediatePositions.Reverse();
//            }
//        }

//        /// <summary>
//        ///     Sets the traffic entities current node property.
//        /// </summary>
//        /// <param name="n"> A new node that denotes the new currentNode property, type Graph.Node.</param>
//        public void SetCurrentNode(Graph.Node n)
//        {
//            this.currentNode = n;
//        }

//        /// <summary>
//        ///     Sets the traffic entities nextNode property, based on its calculated path.
//        /// </summary>
//        public void SetNextNode()
//        {
//            //Finds the current node in the path and sets the next node as the next node along, if there is one
//            for (int i=0; i<path.Count; i++)
//            {
//                if (path[i].Equals(this.currentNode.nodeId) && (i+1)<path.Count)
//                {
//                    this.nextNode = parentGraph.nodeIdNodeDict[path[i+1]];
//                }
//            }
//        }
//    }
//}
