﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Pathing
{
    /// <summary>
    ///     A class that allows us to construct a graph of intersections
    ///     in the map that are defined by the <see cref="Node"/> class.
    /// </summary>
    public class Graph
    {
        // GRAPH PROPERTIES
        public List<Way> ways { get; set; }
        public List<Node> nodes { get; set; }
        public Dictionary<string, Node> nodeIdNodeDict;

        /// <summary>
        ///     Graph() class constructor.
        /// </summary>
        public Graph()
        {
            ways = new List<Way>();
            nodes = new List<Node>();
            nodeIdNodeDict = new Dictionary<string, Node>();
        }

        /// <summary>
        ///     Manually add a node (<see cref="Node"/>) to the graph.
        /// </summary>
        /// <param name="n"> A node to add to the graph. </param>
        public void AddNode(Node n)
        {
            nodes.Add(n);
            nodeIdNodeDict[n.nodeId] = n;
        }

        /// <summary>
        ///     Read in a .Json file that contains the nodes and ways, 
        ///     outputted from the map tool.
        /// </summary>
        /// <param name="fileName"> A .JSON file that contains road data. </param>
        public void ReadInGraphFromJSON(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                dynamic arr = JsonConvert.DeserializeObject(json);

                // Loop through all ways in file
                foreach (var way in arr["ways"])
                {
                    string wayId = way["wayId"];
                    string name = way["name"];
                    string highwayType = way["highwayType"];
                    int maxSpeed = Convert.ToInt32(way["maxSpeed"]);
                    ways.Add(new Way(wayId, name, highwayType, maxSpeed));
                }

                // Loop through all nodes in file
                foreach (var node in arr["nodes"])
                {
                    string nodeId = node["nodeId"];
                    double lat = Convert.ToDouble(node["lat"]);
                    double lon = Convert.ToDouble(node["lon"]);
                    string wayId = node["wayId"];
                    bool isSignalJSON = node["isSignal"];
                    int isSignal;
                    if (isSignalJSON==true)
                    {
                        isSignal=1;
                    }
                    else
                    {
                        isSignal=0;
                    }
                    Node n = new Node(nodeId, wayId, lat, lon, isSignal);

                    // Loop through all neighbours in node
                    foreach (var neighbour in node["neighbours"])
                    {
                        string neighbourId = neighbour["neighbourId"];
                        double distance = Convert.ToDouble(neighbour["distance"]);
                        bool isOneWay = Convert.ToBoolean(neighbour["isOneWay"]);
                        string neighbourWayId = neighbour["wayId"];
                        n.AddNeighbour(neighbourId, distance, isOneWay, neighbourWayId);
                    }
                    nodes.Add(n);
                    nodeIdNodeDict[n.nodeId] = n;
                }
            }
        }

        /// <summary>
        ///     A class that defines a node (a.k.a intersection) in the map.
        ///     Each node has a map coordinate represented as latitude and longitude,
        ///     and also a list of its corresponding neighbours <see cref="Neighbour"/>.
        /// </summary>
        public class Node
        {
            // NODE PROPERTIES
            public string nodeId { get; set; }
            public double lat { get; set; }
            public double lon { get; set; }
            public string wayId { get; set; }
            public int isSignal { get; set; }
            public List<Neighbour> neighbours { get; set; }

            /// <summary>
            ///     Node() class constructor.
            /// </summary>
            /// <param name="nodeId"> The nodes ID value, type string. </param>
            /// <param name="wayId"> The way identifier that the node is associated with. </param>
            public Node(string nodeId, string wayId, double lat, double lon, int isSignal)
            {
                this.nodeId = nodeId;
                this.wayId = wayId;
                this.lat = lat;
                this.lon = lon;
                this.isSignal=isSignal;
                neighbours = new List<Neighbour>();
            }

            /// <summary>
            ///     Adds a single neighbour to a node in the graph, by node id.
            /// </summary>
            /// <param name="neighbourId"> The nodes ID value, type string. </param>       
            /// <param name="distance"> The nodes distance from its parent node, type double. </param>
            /// <param name="isOneWay"> Denotes whether or not the parent node to itself is a one way road, type boolean. </param>
            public void AddNeighbour(string neighbourId, double distance, bool isOneWay, string wayId)
            {
                neighbours.Add(new Neighbour(neighbourId, distance, isOneWay, wayId));
            }

            /// <summary>
            ///     The Neighbour class defines its own coordinates and the
            ///     the associated costs to traverse the path, from the original node.
            /// </summary>
            public class Neighbour
            {
                // NEIGHBOUR PROPERTIES
                public string neighbourId;
                public double distance;
                public bool isOneWay;
                public string wayId;

                /// <summary>
                ///     Neighbour() class constructor.
                /// </summary>
                /// <param name="neighbourId"> The nodes ID value, type string. </param>              
                /// <param name="distance"> The nodes distance from its parent node, type double. </param>
                /// <param name="isOneWay"> Denotes whether or not the parent node to itself is a one way road, type boolean. </param>
                /// <param name="wayId"> The way ID that the parent node to itself belongs to, type string. </param>
                public Neighbour(string neighbourId, double distance, bool isOneWay, string wayId)
                {
                    this.neighbourId = neighbourId;
                    this.distance = distance;
                    this.isOneWay = isOneWay;
                    this.wayId = wayId;
                }
            }
        }

        /// <summary>
        ///     A way is essentially a set of nodes <see cref="Node"/>,
        ///     that make a path. Ways make up each individual 'sub-section' 
        ///     of road in the entire map.
        /// </summary>
        public class Way
        {
            // WAY PROPERTIES
            public string wayId { get; set; }
            public string name { get; set; }
            public string highwayType { get; set; }
            public int maxSpeed { get; set; }

            /// <summary>
            ///     Way() class constructor.
            /// </summary>
            /// <param name="wayId"> The way ID that the parent node to itself belongs to, type string. </param>
            /// <param name="name"> The way's real-world road name, type string. </param>
            /// <param name="highwayType"> The way's road type, type string.</param>
            /// <param name="maxSpeed"> The way's max speed allowed, type int. </param>
            public Way(string wayId, string name, string highwayType, int maxSpeed)
            {
                this.wayId = wayId;
                this.name = name;
                this.highwayType = highwayType;
                this.maxSpeed = maxSpeed;
            }
        }


        //Picks the node with the lowest known distance to it that hasn't been visited yet, basically works around implementing
        // a priority queue
        public string getMinDistNode(Dictionary<string, double> timeTo, List<string> unvisitedNodes)
        {
            if (unvisitedNodes.Count == 0)
            {
                throw new System.ArgumentException("Unvisited nodes list cannot be empty");
            }

            double min = double.PositiveInfinity;
            string minNode = null;

            foreach (string n in unvisitedNodes)
            {
                if (timeTo[n] < min)
                {
                    min = timeTo[n];
                    minNode = n;
                }
            }

            if (minNode == null)
            {
                minNode = unvisitedNodes[0];
            }
            return minNode;
        }
    }
}
