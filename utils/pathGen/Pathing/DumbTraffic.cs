﻿using System;
using System.Collections.Generic;

namespace Pathing
{

    /// <summary>
    ///		Extends <see cref="Traffic"/> class by having the car calculate a route once
    ///		and then follow the path until it reaches the destination 
    /// </summary>
    public class DumbTraffic : BaseTraffic
    {
        public DumbTraffic(Graph graph) : base(graph)
        {
            this.parentGraph = graph;
        }

        /// <summary>
        ///     Implements Yen's algorithm to find the k shortest routes through a graph. Returns a list with K routes.
        /// </summary>
        /// <param name="startID"></param>
        /// <param name="endID"></param>
        /// <param name="num_routes"></param>
        /// <returns></returns>
        public List<List<string>> PathToFrom(string startID, string endID, int num_routes)
        {
            if (!parentGraph.nodeIdNodeDict.ContainsKey(startID) || !parentGraph.nodeIdNodeDict.ContainsKey(endID))
            {
                throw new ArgumentException("StartID and EndID must be valid node IDs for this graph");
            }

            Dictionary<(string, string), double> adjMatrix = MakeAdjacencyMatrix();
            List<string> shortestPath = new List<string>();
            try
            {
                shortestPath = GetShortestPath(startID, endID, new List<string>(), adjMatrix);
            } catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return new List<List<string>>() { new List<string>() { startID } };
            }
                
            List<List<string>> kShortestPaths = new List<List<string>>();
            kShortestPaths.Add(shortestPath);
            List<(List<string>, double)> B = new List<(List<string>, double)>();
            for (int k = 0; k < num_routes - 1; k++)
            {
                for (int i = 0; i < kShortestPaths[k].Count - 2; i++)
                {
                    List<((string, string), double)> removedEdges = new List<((string, string), double)>();
                    string spurNode = kShortestPaths[k][i];
                    List<string> rootPath = getRoot(kShortestPaths[k], spurNode);
                    foreach (List<string> p in kShortestPaths)
                    {
                        Boolean same = true;
                        for (int ii = 0; ii < i; ii++)
                        {
                            //possible indexing error here
                            if (!(rootPath[ii].Equals(p[ii])))
                            {
                                same = false;
                                break;
                            }
                        }
                        if (same)
                        {
                            removedEdges.Add(((p[i], p[i + 1]), adjMatrix[(p[i], p[i + 1])]));
                            adjMatrix[(p[i], p[i + 1])] = double.MaxValue;
                        }
                    }

                    List<string> DoNotUse = new List<string>();
                    foreach (string node in rootPath)
                    {
                        DoNotUse.Add(node);
                    }
                    List<string> spurPath = new List<string>();
                    try
                    {
                        spurPath = GetShortestPath(spurNode, endID, DoNotUse, adjMatrix);
                    } catch (ArgumentException e)
                    {
                        foreach (((string, string), double) removedEdge in removedEdges)
                        {
                            adjMatrix[removedEdge.Item1] = removedEdge.Item2;
                            Console.WriteLine(e.Message);
                        }
                        continue;
                    }
                    
                    List<string> totalPath = rootPath;
                    totalPath.AddRange(spurPath);
                    double totalPathCost = GetPathCost(totalPath, adjMatrix);
                    if (!B.Contains((totalPath, totalPathCost)))
                    {
                        B.Add((totalPath, totalPathCost));
                    }
                    foreach (((string, string), double) removedEdge in removedEdges)
                    {
                        adjMatrix[removedEdge.Item1] = removedEdge.Item2;
                    }
                }
                if (B.Count == 0)
                {
                    break;
                }
                double minCost = B[0].Item2;
                List<string> BShortestPath = B[0].Item1;
                for (int i = 1; i < B.Count; i++)
                {
                    if (B[i].Item2 < minCost)
                    {
                        BShortestPath = B[i].Item1;
                    }
                }
                kShortestPaths.Add(BShortestPath);
                B.Remove((BShortestPath, minCost));
            }
            return kShortestPaths;
        }

        /// <summary>
        ///     Implements Djikstras's algorithm to find the shortest path. Each segment in the map has its distance
        ///     changed by a random amount between -10% and 10%. 
        /// </summary>
        /// <param name="startID"></param>
        /// <param name="endID"></param>
        /// <returns></returns>
        public override List<string> PathToFrom(string startID, string endID)
        {
            if (!parentGraph.nodeIdNodeDict.ContainsKey(startID) || !parentGraph.nodeIdNodeDict.ContainsKey(endID))
            {
                throw new ArgumentException("StartID and EndID must be valid node IDs for this graph");
            }

            List<string> nodeIDs = new List<string>();

            //List of current shortest known travel times to each node from the source
            Dictionary<string, double> timeTo = new Dictionary<string, double>();

            //Tracks which nodes still need to be visited, nodes are removed as they are visited
            List<string> nodesToVisit = new List<string>();

            foreach (Graph.Node n0 in parentGraph.nodes)
            {
                //Distance to all nodes except the source start as infinity
                timeTo[n0.nodeId] = double.PositiveInfinity;
                if (n0.nodeId != startID)
                {
                    nodesToVisit.Add(n0.nodeId);
                }
            }

            timeTo[startID] = 0;
            nodesToVisit.Add(startID);

            //Tracks the node immediately before a node on the currently known shortest path, used to reconstruct the path at the end of the algorithm
            Dictionary<string, string> pathParents = new Dictionary<string, string>();
            pathParents[startID] = null;
            Random rand = new Random();
            double variation = 0.2;
            while (nodesToVisit.Count > 0)
            {
                string v = parentGraph.getMinDistNode(timeTo, nodesToVisit);
                nodesToVisit.Remove(v);

                foreach (Graph.Node.Neighbour neighbour in parentGraph.nodeIdNodeDict[v].neighbours)
                {
                    double mod_dist = neighbour.distance + (neighbour.distance * (rand.NextDouble() * (variation - (-variation) + (-variation))));
                    //if the time to v + time from v to its neighbour is shorted than the shortest known time from source to neighbours, update the shortest time to neighbour
                    if ((timeTo[v] + mod_dist) < timeTo[neighbour.neighbourId])
                    {
                        
                        timeTo[neighbour.neighbourId] = timeTo[v] + mod_dist;
                        pathParents[neighbour.neighbourId] = v;
                    }
                }
            }
            List<string> path = new List<string>();

            // Reconstruct path from end node to start node
            string n = endID;
            while (pathParents.ContainsKey(n) && pathParents[n] != null)
            {
                path.Add(n);
                n = pathParents[n];
            }
            path.Add(startID); // Add the starting point
            path.Reverse(); // Reverse so it goes start->finish
            return path;
        }

        private double GetPathCost(List<string> path, Dictionary<(string, string), double> adjMatrix)
        {
            double cost = 0;
            for (int i = 0; i < path.Count - 1; i++)
            {
                cost += adjMatrix[(path[i], path[i + 1])];
            }
            return cost;
        }

        private Dictionary<(string, string), double> MakeAdjacencyMatrix()
        {
            Dictionary<(string, string), double> adjMatrix = new Dictionary<(string, string), double>();
            foreach (Graph.Node n in parentGraph.nodes)
            {
                foreach (Graph.Node.Neighbour v in n.neighbours)
                {
                    adjMatrix[(n.nodeId, v.neighbourId)] = v.distance;
                }
            }
            return adjMatrix;
        }

        private List<string> getRoot(List<string> parentPath, string spur)
        {
            List<string> rootPath = new List<string>();
            for (int i = 0; i < parentPath.Count; i++)
            {
                if (parentPath[i].Equals(spur))
                {
                    break;
                }
                rootPath.Add(parentPath[i]);
            }
            return rootPath;
        }

        private List<string> GetShortestPath(string startID, string endID, List<string> offLimits, Dictionary<(string, string), double> adjMatrix)
        {
            List<string> nodeIDs = new List<string>();

            //List of current shortest known travel times to each node from the source
            Dictionary<string, double> distTo = new Dictionary<string, double>();

            //Tracks which nodes still need to be visited, nodes are removed as they are visited
            List<string> nodesToVisit = new List<string>();

            foreach (Graph.Node n0 in parentGraph.nodes)
            {
                //Distance to all nodes except the source start as infinity
                distTo[n0.nodeId] = double.PositiveInfinity;
                if (n0.nodeId != startID)
                {
                    nodesToVisit.Add(n0.nodeId);
                }
            }

            distTo[startID] = 0;
            nodesToVisit.Add(startID);

            //Tracks the node immediately before a node on the currently known shortest path, used to reconstruct the path at the end of the algorithm
            Dictionary<string, string> pathParents = new Dictionary<string, string>();
            pathParents[startID] = null;

            while (nodesToVisit.Count > 0)
            {
                string v = parentGraph.getMinDistNode(distTo, nodesToVisit);
                nodesToVisit.Remove(v);
                if (offLimits.Contains(v))
                {
                    continue;
                }
                foreach (Graph.Node.Neighbour neighbour in parentGraph.nodeIdNodeDict[v].neighbours)
                {
                    if (offLimits.Contains(neighbour.neighbourId)) { continue; }
                    //if the time to v + time from v to its neighbour is shorted than the shortest known time from source to neighbours, update the shortest time to neighbour
                    if ((distTo[v] + adjMatrix[(v, neighbour.neighbourId)]) < distTo[neighbour.neighbourId])
                    {
                        distTo[neighbour.neighbourId] = distTo[v] + adjMatrix[(v, neighbour.neighbourId)];
                        pathParents[neighbour.neighbourId] = v;
                    }
                }
            }
            List<string> path = new List<string>();

            // Reconstruct path from end node to start node
            string n = endID;
            while (pathParents.ContainsKey(n) && pathParents[n] != null)
            {
                path.Add(n);
                n = pathParents[n];
            }
            path.Add(startID); // Add the starting point
            path.Reverse(); // Reverse so it goes start->finish

            if (!(path[0].Equals(startID) && path[path.Count - 1].Equals(endID)))
            {
                throw new ArgumentException("There is no path between nodes " + startID + " and " + endID);
            }

            return path;
        }
    }

}