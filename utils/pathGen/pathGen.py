import os
import sys
import argparse
import subprocess as sp

if __name__ == "__main__":
    # Create the parser
    my_parser = argparse.ArgumentParser(
        description='Rebuild the Pathing.dll file for your operating system')

    # Add the arguments
    my_parser.add_argument('-r', '--rebuild', action='store_true',
                           help='True means the script will rebuild the .dll, False means the script will just use the already existing .dll')

    # Execute the parse_args() method
    args = my_parser.parse_args()
    REBUILD_DLL = args.rebuild

    # Builds the release .dll for the pathing C# script if REBUILD_DLL is true
    # Output file should be appear in current working directory as "aiPaths.json"
    cwd = os.getcwd()
    if REBUILD_DLL:
        # Build for macOS
        if sys.platform.startswith('darwin'):
            sp.call(["dotnet", "publish", "Pathing/Pathing.sln", "--configuration", "Release", "-r", "osx-x64", "--self-contained", "true"])
            # dllPath = os.path.join(cwd, "Pathing", "bin", "Release",
            #                        "netcoreapp3.1", "osx-x64", "publish", "Pathing.dll")
            # sp.call(["dotnet", dllPath])

        # Build for linux
        elif sys.platform.startswith('linux'):
            sp.call(["dotnet", "publish", "Pathing/Pathing.sln", "--configuration", "Release", "-r", "linux-x64", "--self-contained", "true"])
            # dllPath = os.path.join(cwd, "Pathing", "bin", "Release",
            #                        "netcoreapp3.1", "linux-x64", "publish", "Pathing.dll")
            # sp.call(["dotnet", dllPath])

        # Build for windows
        elif sys.platform.startswith('win32'):
            sp.call(["dotnet", "publish", "Pathing\\Pathing.sln", "--configuration", "Release", "-r", "win-x64", "--self-contained", "true"])
            # dllPath = os.path.join(cwd, "Pathing", "bin", "Release",
            #                        "netcoreapp3.1", "win-x64", "publish", "Pathing.dll")
            # sp.call(["dotnet", dllPath])

    # Output file should be appear in current working directory as "aiPaths.json"
    else:
        # Run for macOS
        if sys.platform.startswith('darwin'):
            dllPath = os.path.join(cwd, "utils", "pathGen", "Pathing", "bin", "Release",
                                   "netcoreapp3.1", "osx-x64", "publish", "Pathing")
            sp.call([dllPath, "utils/pathGen/Graph/nodes.json", "utils/pathGen/Graph/config.ini"])

        # Run for linux
        elif sys.platform.startswith('linux'):
            dllPath = os.path.join(cwd, "utils", "pathGen", "Pathing", "bin", "Release",
                                   "netcoreapp3.1", "linux-x64", "publish", "Pathing")
            sp.call([dllPath, "utils/pathGen/Graph/nodes.json", "utils/pathGen/Graph/config.ini"])

        # Run for windows
        elif sys.platform.startswith('win32'):
            dllPath = os.path.join(cwd, "utils", "pathGen", "Pathing", "bin", "Release",
                                   "netcoreapp3.1", "win-x64", "publish", "Pathing.exe")
            sp.call([dllPath, "utils/pathGen/Graph/nodes.json", "utils/pathGen/Graph/config.ini"])
