'''
Author: Larry H
- Reads .prefab file, edits required values and generates new .prefab file
- Current values: latlong, zoom, extent (change these in config dictionary).
- Input: Map.prefab, found in \MapBuilder\Assets\Mapbox\Prefabs
- Output: out.prefab (to easily test in Unity, rename this to CitySimulator.unity)

- TODO:
    - error checking
        - ceiling for numerics
        - explicit case for null/empty
    - comments
    - how does this file take inputs?
'''

import os
import json
import configparser

# input file
input_prefab = 'Map.prefab'
# output file
output_prefab = 'out.prefab'
# input json
input_config = 'config.ini'
# edit this for values to change
config = {}
config_keys = []
config_write = {}

def enter_config():
    global config, config_keys, config_write
    try:
        #f = open(input_config, 'r') #
        data = configparser.ConfigParser()
        data.read(input_config)
    except:
        raise Exception(
            'No file: {} exists in current directory'.format(input_config))

    #data = json.load(f) #
    # config['latitudeLongitude:'] = '{}, {}'.format(
    #     data[0]['Latitude'], data[0]['Longitude'])
    # config['zoom:'] = int(data[0]['Zoom'])
    # config['west:'] = int(data[0]['Extent'])
    # config['north:'] = int(data[0]['Extent'])
    # config['east:'] = int(data[0]['Extent'])
    # config['south:'] = int(data[0]['Extent'])

    config['latitudeLongitude:'] = '{}, {}'.format(data['MAP_ATTRIBUTES']['latitude'], data['MAP_ATTRIBUTES']['longitude'])
    config['zoom:'] = int(data['MAP_ATTRIBUTES']['zoom'])
    config['west:'] = int(data['MAP_ATTRIBUTES']['extent'])
    config['north:'] = int(data['MAP_ATTRIBUTES']['extent'])
    config['east:'] = int(data['MAP_ATTRIBUTES']['extent'])
    config['south:'] = int(data['MAP_ATTRIBUTES']['extent'])


    config_keys = [key for key in list(config.keys())]
    config_write = {
        'latitudeLongitude:': '      latitudeLongitude: {}'.format(config['latitudeLongitude:']),
        'zoom:': '      zoom: {}'.format(config['zoom:']),
        'west:': '          west: {}'.format(config['west:']),
        'north:': '          north: {}'.format(config['north:']),
        'east:': '          east: {}'.format(config['east:']),
        'south:': '          south: {}'.format(config['south:'])
    }


def validate_config():

    # check for nulls
    if(not not [nulls for nulls in config.values() if nulls is None]):
        raise Exception('Config - Invalid format: values must not be null')

    # check for valid latlong input (syntax)
    latlong_whitespaces = [
        char for char in config['latitudeLongitude:'] if(char.isspace())]
    if(not isinstance(config['latitudeLongitude:'], str)):
        raise Exception(
            'Config - Invalid format: \'latitudeLongitude\', must be of type <string>')
    if(len(latlong_whitespaces) != 1):
        raise Exception(
            'Config - Invalid format: \'latitudeLongitude: {}\'; white_spaces != 1, check for correct syntax (lat, long)'.format(config['latitudeLongitude:']))
    if(len(config['latitudeLongitude:']) < 2):
        raise Exception(
            'Config - Invalid format: \'latitudeLongitude\', must be of proper length')

    # check for valid latlong value
    latlong = config['latitudeLongitude:'].split(', ')
    try:
        latitude, longitude = float(latlong[0]), float(latlong[1])
    except:
        raise Exception(
            'Config - Invalid format: \'latitudeLongitude: {}\'; must contain only numbers'.format(config['latitudeLongitude:']))
    if(latitude < -90 or latitude > 90):
        raise Exception(
            'Config - Invalid format: \'latitude: {}\'; value must be within +-90'.format(latitude))
    if(longitude < -180 or longitude > 180):
        raise Exception(
            'Config - Invalid format: \'longitude: {}\'; value must be within +-180'.format(longitude))

    # check for valid extent and zoom input (value and syntax)
    if(not isinstance(config['zoom:'], int) or not isinstance(config['west:'], int) or not isinstance(config['north:'], int) or not isinstance(config['east:'], int) or not isinstance(config['south:'], int)):
        raise Exception(
            'Config - Invalid format: numerics must be of type <int>')
    if(config['zoom:'] < 1 or config['west:'] < 1 or config['north:'] < 1 or config['east:'] < 1 or config['south:'] < 1):
        raise Exception(
            'Config - Invalid format: numerics must not be less than 1')


def parser(filepath):
    result = str()
    try:
        f = open(input_prefab, 'r')
    except:
        raise Exception(
            'No file: {} exists in current directory'.format(input_prefab))
    # read each line in input, perform edit
    for num, line in enumerate(f.readlines()):
        # check for valid prefab
        # TODO (later) - need better indicators for prefabs, also need to know input shape
        if(num == 0 and '%YAML 1.1\n' != line):
            raise Exception('Not a valid prefab: {}'.format(line))
        if(num == 1 and '%TAG !u! tag:unity3d.com,2011:\n' != line):
            raise Exception('Not a valid prefab: {}'.format(line))

        if any(key in line for key in config_keys):
            key = [key for key in config_keys if key in line]
            line = line_create(key[0])
            result += line
        else:
            result += line
    f.close()
    return result


def line_create(key):
    if(key in config_write.keys()):
        return config_write.get(key) + '\n'

if __name__ == "__main__":
    enter_config()
    validate_config()
    result = parser(input_prefab)
    # output file
    f = open(output_prefab, "w")
    f.write(result)
    f.close()