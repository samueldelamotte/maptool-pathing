import json
import configparser

NAME = "Test"
OUTPUT_DEST = "../wwwroot/webgl/output"
EXTENT = 4
LAT = -31.9521
LON = 115.8605
ZOOM = 16
DEFAULT_SPEED = 50.0
ALLOW_ONE_WAY = True


if __name__ == "__main__":
    map_config = configparser.ConfigParser()
    map_config["MAP_ATTRIBUTES"] = {
        'Name': NAME,
        'OutputDest': OUTPUT_DEST,
        'Latitude': LAT,
        'Longitude': LON,
        'Extent': EXTENT,
        'Zoom': ZOOM,
        'DefaultSpeed': DEFAULT_SPEED,
        'AllowOneWay': ALLOW_ONE_WAY
    }
    with open('example.ini', 'w') as configfile:
        map_config.write(configfile)