"""
basic program,
reads from out.txt (see highways.py) and prints dictionary of each highway type and how many of that type appear in the file.
"""

f = open('out.txt','r')
highways = {}
for line in f.readlines():
    if 'Highway' in line:
        l = line.strip().split()
        if l[1] in highways:
            highways[l[1]] += 1
        else:
            highways[l[1]] = 1
print(highways)