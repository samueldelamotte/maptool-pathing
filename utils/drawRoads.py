import turtle
import highways as hw

"""
Draws roads in a given area using overpass nodes + ways

##### HOW TO USE ##### 
1) make sure you have the required libraries (turtle, openpy)
2) adjust settings:
SIZE -> size of the drawing of map (side lenght of square in px)
EXTENT -> radius of number of tiles drawn around center tile.
            higher extent means more map, note this is an exponential scale.
            eg: 0 = 1-tile drawn, 1 = 9-tiles drawn
LAT + LON -> coordinates at center of the map
ZOOM -> dictates size of the tiles, more zoom means higher details,
        for best results, keep at 15 or 16

turtle.tracer -> show drawing or jump to finished image.
                True (or comment out) = show drawing

3) run in same folder as overtest.py
"""

SIZE = 680
EXTENT = 2
LAT = 40.69061
LON = -73.945242
ZOOM = 16

turtle.tracer(False)

turtle.screensize(SIZE+100,SIZE+100)
turtle.bgcolor('gray')
halfSize = SIZE/2
t = turtle.Turtle()

t.up()
t.goto(halfSize,halfSize)
t.down()
t.goto(halfSize,-halfSize)
t.goto(-halfSize,-halfSize)
t.goto(-halfSize,halfSize)
t.goto(halfSize,halfSize)

t.up()

cols = {
    'motorway': 'purple',
    'motorway_link': 'purple',
    'trunk': 'green',
    'trunk_link': 'green',
    'primary': 'red',
    'primary_link': 'red',
    'secondary': 'orange',
    'secondary_link': 'orange',
    'tertiary': 'yellow',
    'tertiary_link': 'yellow',
    'unclassified': 'black',
    'residential': 'white',
    'crossing': 'olive',
    'traffic_signals': 'brown.
    '
}
layer_cols = {
    '-1': 'lime',
    '1': 'blue',
    '2': 'cyan'
}

def toScreen(lon,lat, MINLON,LONDIF,MINLAT,LATDIF):
    xStep = lon-MINLON
    newX = ((xStep/LONDIF)*SIZE)-halfSize

    yStep = lat-MINLAT
    newY = ((yStep/LATDIF)*SIZE)-halfSize

    return newX, newY

def main():
    tl,br = hw.getBox(LAT,LON,ZOOM,EXTENT)
    result = hw.getWays(LAT,LON,ZOOM,EXTENT)

    MINLON = tl[1]
    LONDIF = br[1]-tl[1]
    MINLAT = br[0]
    LATDIF = tl[0]-br[0]

    t.up()
    for way in result:
        highwayType = way.tags.get("highway", "n/a")
        layer = way.tags.get("layer", "n/a")
        print(highwayType)
        t.color(cols[highwayType])
        if layer != 'n/a':
            t.color(layer_cols[layer])

        for node in way.nodes:
            nodeHighwayType = node.tags.get("highway", "n/a")
            t.goto(toScreen(float(node.lon),float(node.lat), MINLON,LONDIF,MINLAT,LATDIF))
            t.down()
            if nodeHighwayType in ['crossing','traffic_signals']:
                t.color(cols[nodeHighwayType])
                t.dot(size=5)
                t.color(cols[highwayType])
        t.up()


if (__name__ == "__main__"):
    main()
    turtle.Screen().exitonclick()
