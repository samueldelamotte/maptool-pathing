import overpy
import math

"""
Prints out a list of all "highways" (all footpaths/roads/etc) in a given area.
I was calling this as: 
    python highways.py > out.txt
so that I could read the results in a text editor (to use cmd+f)
but it still works if you call it normally.

Make sure you have overpy installed

EXTENT -> radius of number of tiles drawn around center tile.
        higher extent means more map, note this is an exponential scale.
        eg: 0 = 1-tile drawn, 1 = 9-tiles drawn
LAT + LON -> coordinates at center of the map
ZOOM -> dictates size of the tiles, more zoom means higher details,
        for best results, keep at 15 or 16
"""

api = overpy.Overpass()
EXTENT = 4 
LAT = -31.9627
LON = 115.8482
ZOOM = 16

DRAW_WAYS = [
    'motorway', 'motorway_link',
    'trunk', 'trunk_link',
    'primary','primary_link',
    'secondary', 'secondary_link',
    'tertiary', 'tertiary_link',
    'unclassified',
    'residential',
    'crossing']

def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.asinh(math.tan(lat_rad)) / math.pi) / 2.0 * n)
  return (xtile, ytile)

def num2deg(xtile, ytile, zoom):
  n = 2.0 ** zoom
  lon_deg = xtile / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return (lat_deg, lon_deg)

def getBox(lat,lon,zoom,extent=0):
    xtile,ytile = deg2num(lat,lon,zoom)
    tl = num2deg(xtile-extent,ytile-extent,zoom)
    br = num2deg(xtile+1+extent,ytile+1+extent,zoom)
    return tl,br
    #return 'way('+ str(br[0]) +', '+ str(tl[1]) +', '+ str(tl[0]) +', '+ str(br[1]) +')'


def getWays(lat,lon,zoom,extent):
    output_ways = []
    tl,br = getBox(lat,lon,zoom,extent)
    result = api.query('way('+ str(br[0]) +', '+ str(tl[1]) +', '+ str(tl[0]) +', '+ str(br[1]) +") ['highway']; (._;>;); out body;")
    for way in result.ways:
        highwayType = way.tags.get("highway", "n/a")
        if highwayType in DRAW_WAYS:
            output_ways.append(way)
    return output_ways


def main():
    # fetch all ways and nodes
    # query way(bl-lat, bl-lon, tr-lat, tr-lon)
    # query way(br-lat, tl-lon, tl-lat, br-lon)
    result = getWays(LAT,LON,ZOOM,EXTENT)

    
    for way in result:
        print("Name: %s" % way.tags.get("name", "n/a"))
        print("  Highway: %s" % way.tags.get("highway", "n/a"))
        if way.tags.get("layer", "n/a") != "n/a":
            print("  has layer: %s" % way.tags.get("layer", "n/a"))
            print(way.tags)

        print("  Nodes:")
        for node in way.nodes:
            print("    Lat: %f, Lon: %f" % (node.lat, node.lon))


if (__name__ == "__main__"):
    main()
