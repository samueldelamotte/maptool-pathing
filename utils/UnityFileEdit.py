'''
Author: Larry H
- Reads .unity file, edits required values and generates new .unity file
- Current values: latlong, zoom, extent (change these in _config dictionary). Need to look at other things to edit (e.g. data source, lighting, etc.)
- Input: CitySimulator_og.unity (example CitySimulator prefab from MapBox, I renamed it here)
- Output: out.unity (to easily test in Unity, rename this to CitySimulator.unity)
'''

import yaml
import os

_config = {
    'latlong': "-31.565, 115.8605",
    'zoom': 4234,
    'extent': {
        'north': 234,
        'south': 656,
        'east': 555,
        'west': 8978
    },
}

file_path = 'CitySimulator_og.unity'

def parser(filepath):
    result = str()
    f = open(file_path, 'r')
    flag = False
    flag_string = str()

    switch = {
        '_options.locationOptions.latitudeLongitude': 'latlong',
        '_options.locationOptions.zoom': 'zoom',
        '_options.extentOptions.defaultExtents.rangeAroundCenterOptions.east': 'extent.east',
        '_options.extentOptions.defaultExtents.rangeAroundCenterOptions.west': 'extent.west',
        '_options.extentOptions.defaultExtents.rangeAroundCenterOptions.north': 'extent.north',
        '_options.extentOptions.defaultExtents.rangeAroundCenterOptions.south': 'extent.south'
    }

    switch_keys = [key for key in list(switch.keys())]
    for num, line in enumerate(f.readlines()): 
        if(flag):
            line = flag_handler(flag_string)
            result += line
            flag = False
            flag_string = str()
        elif any(key in line for key in switch_keys):
            key = [key for key in switch_keys if key in line]
            flag = True
            print(key)
            flag_string = switch.get(key[0])
            result += line
        else:
            result += line
    f.close() 
    return result

def flag_handler(flag_string):
    print(flag_string)
    switch = {
        'latlong': _config['latlong'],
        'zoom': _config['zoom'],
        'extent.east': _config['extent']['east'],
        'extent.west': _config['extent']['west'],
        'extent.north': _config['extent']['north'],
        'extent.south': _config['extent']['south']
    }
    if(flag_string in switch.keys()):
        return '      value: {}\n'.format(switch.get(flag_string))

result = parser(file_path)

f = open("out.unity", "w")
f.write(result)
f.close()