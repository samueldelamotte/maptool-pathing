// GLOBALS 
var map;
var bbox = null;

function initialize(){
    map = L.map('map').setView([-31.950527, 115.860458], 14);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        minZoom: 10,
        maxZoom: 18
    }).addTo(map);

    bbox = L.rectangle([[-31.938178485591138,115.8453369140625],[-31.961483557268544,115.872802734375]]).addTo(map);

    new QWebChannel(qt.webChannelTransport, function (channel) {
        window.GetMapDataWindow = channel.objects.GetMapDataWindow;
        if(typeof GetMapDataWindow != 'undefined') {
            // MAP MOVE HANDLER --------------------------------------------------------------------------------------------
            var onMapMove = function() { 
                GetMapDataWindow.onMapMove(map.getCenter().lat, map.getCenter().lng) ;
            };
            map.on('move', onMapMove);
            onMapMove();
        }
    });
}