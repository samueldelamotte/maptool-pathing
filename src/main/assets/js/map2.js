// GLOBALS 
var map;
var originMarker = null;
var destinationMarker = null;
var spawnCircle = null;
var joinLine = null;
var pairNum = 1;
var playerPairNum = 1;
var trafficPopPercentRemaining = 100.0;
var playerODs = [];
var numPlayerSessions = 1;

function initialize(){
    map = L.map('map').setView([-31.950527, 115.860458], 14);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        minZoom: 10,
        maxZoom: 18
    }).addTo(map);

    // HELPER FUNCTIONS ------------------------------------------------------------------------------------------
    String.prototype.format = function() {
        a = this;
        for (k in arguments) {
          a = a.replace("{" + k + "}", arguments[k]);
        }
        return a;
    };

    function hexToRGB(hex, alpha) {
        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16);
        if (alpha) {
            return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
        } else {
            return "rgb(" + r + ", " + g + ", " + b + ")";
        }
    }

    new QWebChannel(qt.webChannelTransport, function (channel) {
        window.OriginDestinationWindow = channel.objects.OriginDestinationWindow;
        if(typeof OriginDestinationWindow != 'undefined') {
            // MAP LOADED HANDLER --------------------------------------------------------------------------------------------
            var loadFinishedCallBack = function () {
                OriginDestinationWindow.onMapLoad();
            };
            map.whenReady(loadFinishedCallBack);

            // MAP CLICK HANDLER -------------------------------------------------------------------------------------------
            var originLatLng = [];
            var destinationLatLng = [];
            var radius = null;
            var trafficPopPercent = null;
            var task = "playerOrigin";
            var onMapClick = function(e) { 
                if (e != null) {
                    switch (task) {
                        case "playerOrigin":
                            if (numPlayerSessions != 0) {
                                originMarker = L.marker(e.latlng, {icon: originIcon}).addTo(map);
                                originMarker.bindPopup("PLAYER ORIGIN - [{0}]<br />Lat: {1}, Lng: {2}".format(playerPairNum.toString(), e.latlng.lat, e.latlng.lat));
                                originLatLng.push(e.latlng.lat, e.latlng.lng);
                                task = "playerDestination";
                            }
                            break;
                        case "playerDestination":
                            if (numPlayerSessions != 0) {
                                destinationMarker = L.marker(e.latlng, {icon: destinationIcon}).addTo(map);
                                destinationMarker.bindPopup("PLAYER DESTINATION - [{0}]<br />Lat: {1}, Lng: {2}".format(playerPairNum.toString(), e.latlng.lat, e.latlng.lat));
                                destinationLatLng.push(e.latlng.lat, e.latlng.lng);
                                playerPairNum += 1;
                                numPlayerSessions -= 1;
                                joinLine = L.polyline([originLatLng, destinationLatLng]).addTo(map);
                                OriginDestinationWindow.updateConfigTextBox("[PLAYER_SESSION_{0}]\norigin = [{1},{2}]\ndestination = [{3},{4}]\n\n".format((playerPairNum-1).toString(), originLatLng[0].toFixed(5).toString(), originLatLng[1].toFixed(5).toString(), destinationLatLng[0].toFixed(5).toString(), destinationLatLng[1].toFixed(5).toString()), -1)
                                originLatLng = [];
                                destinationLatLng = [];
                                if (numPlayerSessions == 0) {
                                    task = "origin";
                                    OriginDestinationWindow.enableRemoveODPairBtn();
                                } else {
                                    task = "playerOrigin"
                                }
                            }
                            break;
                        case "origin":
                            if (trafficPopPercentRemaining > 0.0) {
                                originMarker = L.marker(e.latlng, {icon: originIcon}).addTo(map);
                                originMarker.bindPopup("ORIGIN - [{0}]<br />Lat: {1}, Lng: {2}".format(pairNum.toString(), e.latlng.lat, e.latlng.lat));
                                originLatLng.push(e.latlng.lat, e.latlng.lng);
                                task = "destination";
                            } else {
                                alert('No traffic population remaining!')
                            }
                            break;
                        case "destination":
                            destinationMarker = L.marker(e.latlng, {icon: destinationIcon}).addTo(map);
                            destinationMarker.bindPopup("DESTINATION - [{0}]<br />Lat: {1}, Lng: {2}".format(pairNum.toString(), e.latlng.lat, e.latlng.lat));
                            destinationLatLng.push(e.latlng.lat, e.latlng.lng);
                            destination = false;
                            pairNum += 1;
                            var maxRadius = originMarker.getLatLng().distanceTo(destinationMarker.getLatLng())
                            if (e.latlng.lat != originLatLng[0] && e.latlng.lng != originLatLng[1] && trafficPopPercentRemaining > 0.0) {
                                try {
                                    var radiusValid = false;
                                    while (!radiusValid) {
                                        radius = window.prompt("Max radius = {0}\nEnter origin spawn radius: ".format(maxRadius), "{0}".format(maxRadius));
                                        if (parseFloat(radius) <= maxRadius && parseFloat(radius) > 0.0) {radiusValid = true};
                                    }
                                    var trafficPopPercentValid = false;
                                    while (!trafficPopPercentValid) {
                                        trafficPopPercent = window.prompt("Enter traffic population %: ");
                                        if (parseFloat(trafficPopPercent) <= 100.0 && parseFloat(trafficPopPercent) >= 0.0 && trafficPopPercent <= trafficPopPercentRemaining) {trafficPopPercentValid = true}
                                    }
                                    trafficPopPercentRemaining -= trafficPopPercent
                                    spawnCircle = L.circle(originLatLng, parseFloat(radius), circleOptions).addTo(map);
                                    joinLine = L.polyline([originLatLng, destinationLatLng]).addTo(map);
                                    OriginDestinationWindow.updateConfigTextBox("[OD_PAIR_{0}]\norigin = [{1},{2}]\ndestination = [{3},{4}]\nspawn_radius = {5}\ntraffic_pop_percent = {6}\n\n".format((pairNum-1).toString(), originLatLng[0].toFixed(5).toString(), originLatLng[1].toFixed(5).toString(), destinationLatLng[0].toFixed(5).toString(), destinationLatLng[1].toFixed(5).toString(), radius, trafficPopPercent), trafficPopPercent)
                                } catch(e) {
                                    alert(e);
                                    map.removeLayer(originMarker);
                                    map.removeLayer(destinationMarker);
                                };
                            } else {
                                map.removeLayer(originMarker);
                                map.removeLayer(destinationMarker);
                                pairNum -= 1;
                            }
                            originLatLng = [];
                            destinationLatLng = [];
                            task = "origin"
                            break;
                    }
                };
            };
            map.on('click', onMapClick);
            onMapClick();

            // LEAFLET CONFIG ------------------------------------------------------------------------------------------
            var originIcon = new L.Icon({
                iconUrl: './images/marker-icon-blue.png',
                shadowUrl: './images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var destinationIcon = new L.Icon({
                iconUrl: './images/marker-icon-red.png',
                shadowUrl: './images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            var circleOptions = {
                color: 'blue',
                fillColor: hexToRGB("808080", "0.15"),
                fillOpacity: 100
            };
        }
    });
}